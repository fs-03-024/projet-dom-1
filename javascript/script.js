// Récupérer tous les éléments nécessaires
const quantityButtons = document.querySelectorAll('.fa-plus-circle, .fa-minus-circle');
const deleteButtons = document.querySelectorAll('.fa-trash-alt');
const likeButtons = document.querySelectorAll('.fa-heart');
const totalElement = document.querySelector('.total');

// Ajouter des gestionnaires d'événements aux boutons de quantité
quantityButtons.forEach(button => {
  button.addEventListener('click', () => {
    const cardBody = button.closest('.card-body');
    const quantityElement = cardBody.querySelector('.quantity');
    const unitPriceElement = cardBody.querySelector('.unit-price');
    const total = parseInt(totalElement.innerText.slice(0, -2));
    const unitPrice = parseInt(unitPriceElement.innerText);
    let quantity = parseInt(quantityElement.innerText);

    if (button.classList.contains('fa-plus-circle')) {
      quantity++;
    } else if (button.classList.contains('fa-minus-circle') && quantity > 0) {
      quantity--;
    }

    quantityElement.innerText = quantity;
    updateTotal(total, unitPrice);
  });
});

// Ajouter des gestionnaires d'événements aux boutons de suppression
deleteButtons.forEach(button => {
  button.addEventListener('click', () => {
    const cardBody = button.closest('.card-body');
    const quantityElement = cardBody.querySelector('.quantity');
    const unitPriceElement = cardBody.querySelector('.unit-price');
    const total = parseInt(totalElement.innerText.slice(0, -2));
    const unitPrice = parseInt(unitPriceElement.innerText);
    const quantity = parseInt(quantityElement.innerText);

    cardBody.parentNode.removeChild(cardBody);
    updateTotal(total, unitPrice * quantity);
  });
});

// Ajouter des gestionnaires d'événements aux boutons de like
likeButtons.forEach(button => {
  button.addEventListener('click', () => {
    button.classList.toggle('liked');
  });
});

// Met à jour le prix total
function updateTotal(total, amount) {
  total += amount;
  totalElement.innerText = total + ' $';
}